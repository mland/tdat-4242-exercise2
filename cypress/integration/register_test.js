describe('Register Page Test', () => {
    beforeEach(() => {
        cy.visit('http://localhost:9090/register.html')

    })

    it('Test input blank', () => {
        cy.get('#btn-create-account').click()

        cy.get('.alert').should('be.visible').should('contain', 'Registration failed!').should('contain', 'This field may not be blank.');

    })

    it('Test username input', () => {
        let input = '';
        for(let i = 0; i < 151; i++){
            input+='a';
        }

        cy.get('input[name="username"]')
        .type(input)
        .should('have.value', input)

        cy.get('input[name="email"]')
        .type('mail@mail.com')
        .should('have.value', 'mail@mail.com')

        cy.get('input[name="password"]')
        .type('passord123')
        .should('have.value', 'passord123')

        cy.get('input[name="password1"]')
        .type('passord123')
        .should('have.value', 'passord123')

        cy.get('input[name="phone_number"]')
        .type('12345678')
        .should('have.value', '12345678')

        cy.get('input[name="country"]')
        .type('Norway')
        .should('have.value', 'Norway')

        cy.get('input[name="city"]')
        .type('Trondheim')
        .should('have.value', 'Trondheim')

        cy.get('input[name="street_address"]')
        .type('Road 2b')
        .should('have.value', 'Road 2b')

        cy.get('#btn-create-account').click()

        cy.get('.alert').should('be.visible').should('contain', 'Ensure this field has no more than 150 characters.');

    })

    it('Test mail input', () => {
        cy.get('input[name="username"]')
        .type('user')

        cy.get('input[name="email"]')
        .type('mail.com')

        cy.get('input[name="password"]')
        .type('passord123')

        cy.get('input[name="password1"]')
        .type('passord123')

        cy.get('input[name="phone_number"]')
        .type('12345678')

        cy.get('input[name="country"]')
        .type('Norway')

        cy.get('input[name="city"]')
        .type('Trondheim')

        cy.get('input[name="street_address"]')
        .type('Road 2b')

        cy.get('#btn-create-account').click()

        cy.get('.alert').should('be.visible').should('contain', 'Enter a valid email address.');

    })

    it('Test password input not equal', () => {
        cy.get('input[name="username"]')
        .type('user4')

        cy.get('input[name="email"]')
        .type('mail@mail.com')

        cy.get('input[name="password"]')
        .type('123passord')

        cy.get('input[name="password1"]')
        .type('passord123')

        cy.get('input[name="phone_number"]')
        .type('12345678')

        cy.get('input[name="country"]')
        .type('Norway')

        cy.get('input[name="city"]')
        .type('Trondheim')

        cy.get('input[name="street_address"]')
        .type('Road 2b')

        cy.get('#btn-create-account').click()

        cy.get('.alert').should('be.visible').should('contain', 'Ensure password fields match.');
    })

    it('Test phone number input', () => {
        let input = '';
        for(let i = 0; i < 51; i++){
            input+='1';
        }

        cy.get('input[name="username"]')
        .type('user4')

        cy.get('input[name="email"]')
        .type('mail@mail.com')

        cy.get('input[name="password"]')
        .type('123passord')

        cy.get('input[name="password1"]')
        .type('passord123')

        cy.get('input[name="phone_number"]')
        .type(input)

        cy.get('input[name="country"]')
        .type('Norway')

        cy.get('input[name="city"]')
        .type('Trondheim')

        cy.get('input[name="street_address"]')
        .type('Road 2b')

        cy.get('#btn-create-account').click()

        cy.get('.alert').should('be.visible').should('contain', 'Ensure this field has no more than 50 characters.');
    })

    it('Test Country input', () => {
        let input = '';
        for(let i = 0; i < 51; i++){
            input+='a';
        }

        cy.get('input[name="username"]')
        .type('user4')

        cy.get('input[name="email"]')
        .type('mail@mail.com')

        cy.get('input[name="password"]')
        .type('123passord')

        cy.get('input[name="password1"]')
        .type('passord123')

        cy.get('input[name="phone_number"]')
        .type('12345678')

        cy.get('input[name="country"]')
        .type(input)

        cy.get('input[name="city"]')
        .type('Trondheim')

        cy.get('input[name="street_address"]')
        .type('Road 2b')

        cy.get('#btn-create-account').click()

        cy.get('.alert').should('be.visible').should('contain', 'Ensure this field has no more than 50 characters.');
    })

    it('Test City input', () => {
        let input = '';
        for(let i = 0; i < 51; i++){
            input+='a';
        }

        cy.get('input[name="username"]')
        .type('user4')

        cy.get('input[name="email"]')
        .type('mail@mail.com')

        cy.get('input[name="password"]')
        .type('123passord')

        cy.get('input[name="password1"]')
        .type('passord123')

        cy.get('input[name="phone_number"]')
        .type('12345678')

        cy.get('input[name="country"]')
        .type('Norway')

        cy.get('input[name="city"]')
        .type(input)

        cy.get('input[name="street_address"]')
        .type('Road 2b')

        cy.get('#btn-create-account').click()

        cy.get('.alert').should('be.visible').should('contain', 'Ensure this field has no more than 50 characters.');
    })

    it('Test Street address input', () => {
        let input = '';
        for(let i = 0; i < 51; i++){
            input+='a';
        }

        cy.get('input[name="username"]')
        .type('user4')

        cy.get('input[name="email"]')
        .type('mail@mail.com')

        cy.get('input[name="password"]')
        .type('123passord')

        cy.get('input[name="password1"]')
        .type('passord123')

        cy.get('input[name="phone_number"]')
        .type('12345678')

        cy.get('input[name="country"]')
        .type('Norway')

        cy.get('input[name="city"]')
        .type('Trondheim')

        cy.get('input[name="street_address"]')
        .type(input)

        cy.get('#btn-create-account').click()

        cy.get('.alert').should('be.visible').should('contain', 'Ensure this field has no more than 50 characters.');
    })
  })
