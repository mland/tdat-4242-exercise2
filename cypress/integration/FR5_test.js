

//Note a user canot see there own workouts publiched with the coach visabilety and a user cannot see there own workouts with private visabilety.
//one cannot comment on ther workouts than your own, which take the point away ree.

Cypress.Commands.add('login', (username, password) => {
    cy.visit('https://secfit-frontend-ml.herokuapp.com/login.html')
    //cy.contains('Log in').click()
    cy.get('input[name="username"]').type(username)
    cy.get('input[name="password"]').type(password)
    cy.get('#form-login')
    .get('#btn-login').click()
  
  })
  
  describe('Test for exercise', () => {
      
      beforeEach('Visits the secfit page on heroku', () => {
        
        cy.login('a', 'a')
             
          
    })

     
    it('in the my workout tab wet test if a user can see all the fields one of there own workouts', () => {
        cy.wait(1000)
        cy.contains('My Workouts').click()
        cy.wait(100)
        cy.contains('jumpydumpy').click()
        //cy.get('#div-content').find('.list-group-item').first().click()
        
        cy.get('#inputName').should('be.visible').should('have.value','jumpydumpy');
        cy.get('#inputDateTime').should('be.visible')

        //Test here if curret user is the user who owns this workout
        cy.get('#inputOwner').should('be.visible').should('have.value','a');

        cy.get('#inputVisibility').should('be.visible').should('have.value','PU');
        cy.get('#inputNotes').should('be.visible').should('have.value','Run poms');
        cy.get('#customFile').should('be.visible')
        cy.get('#comment-area').should('be.visible')
        cy.get('#comment-list').contains('hi')
    })


    it('in the public workout tab we test if a user can see all the fields one of a random users public workout', () => {
        cy.wait(1000)
        cy.contains('Public Workouts').click()
        cy.wait(100)
        cy.contains('Swimming').click()
        
        cy.get('#inputName').should('be.visible').should('have.value','Swimming');
        cy.get('#inputDateTime').should('be.visible')

        //Test here if curret user is the user who owns this workout
        cy.get('#inputOwner').should('be.visible').should('have.value','as');

        cy.get('#inputVisibility').should('be.visible').should('have.value','PU');
        cy.get('#inputNotes').should('be.visible').should('have.value','10 reps');
        cy.get('#customFile').should('be.visible')
        cy.get('#comment-area').should('be.visible')
    })


    it('in My workout tab we try to find a privat workout that is created for this user. This is done manualy to not create a new workout every test', () => {
        cy.wait(1000)
        cy.contains('My Workouts').click()
        cy.wait(100)
        cy.contains('my private workout')       
    })


    

    it('in the Athlete workout tab we test if a coach can see all the fields one of there atheletes', () => {
        cy.wait(1000)
        cy.contains('Visible to Coach').click()
        cy.wait(100)
        //cy.get('#div-content').find('.list-group-item').first().click()
        
        cy.get('#inputName').should('be.visible').should('have.value','Visible to Coach');
        cy.get('#inputDateTime').should('be.visible')

        //Test here if curret user is the user who owns this workout
        cy.get('#inputOwner').should('be.visible').should('have.value','maria');

        cy.get('#inputVisibility').should('be.visible').should('have.value','CO');
        cy.get('#inputNotes').should('be.visible').should('have.value','asd');
        cy.get('#customFile').should('be.visible')
        cy.get('#comment-area').should('be.visible')
    
    })

    

})
  