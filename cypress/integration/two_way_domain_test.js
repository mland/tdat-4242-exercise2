describe('Register Page Test', () => {
    beforeEach(() => {
        
        cy.visit('https://secfit-frontend-ml.herokuapp.com/register.html')

        
    })

    it('Test input blank', () => {
        cy.get('#btn-create-account').click()

        cy.get('.alert').should('be.visible').should('contain', 'Registration failed!').should('contain', 'This field may not be blank.');

    })

    // Testing the if you can create an account without the different input. U are not suposed to be able to create without username, and password.
    // The rest of the fields are not required.

    //Testing if you can create a user without a username.
    it('Test user input blank', () => {
       
        cy.get('input[name="email"]')
        .type('mail@mail.com')

        cy.get('input[name="password"]')
        .type('passord123')

        cy.get('input[name="password1"]')
        .type('passord123')

        cy.get('input[name="phone_number"]')
        .type('12345678')

        cy.get('input[name="country"]')
        .type('Norway')

        cy.get('input[name="city"]')
        .type('Trondheim')

        cy.get('input[name="street_address"]')
        .type('Road 2b')

        cy.get('#btn-create-account').click()
        cy.get('.alert').should('be.visible').should('contain', 'Registration failed!').should('contain','username').should('contain', 'This field may not be blank.');
    })

    it('Test user existing user', () => {
        cy.get('input[name="username"]')
        .type('a')

        cy.get('input[name="email"]')
        .type('mail@mail.com')

        cy.get('input[name="password"]')
        .type('passord123')

        cy.get('input[name="password1"]')
        .type('passord123')

        cy.get('input[name="phone_number"]')
        .type('12345678')

        cy.get('input[name="country"]')
        .type('Norway')

        cy.get('input[name="city"]')
        .type('Trondheim')

        cy.get('input[name="street_address"]')
        .type('Road 2b')

        cy.get('#btn-create-account').click()
        cy.get('.alert').should('be.visible').should('contain', 'Registration failed!');
    })


    //Testing the system without an email at registration
    //This test fails beacause it is possible to make an accouunt without an email.
    //we beleve this should not be possible as then there is no way for a user to recover their account.

    it('Test email input blank', () => {
        let user ='_' + Math.random().toString(36).substr(2, 9);

        cy.get('input[name="username"]')
        .type(user)
       
        /*
        cy.get('input[name="email"]')
        .type('mail@mail.com')
        */

        cy.get('input[name="password"]')
        .type('passord123')

        cy.get('input[name="password1"]')
        .type('passord123')

        cy.get('input[name="phone_number"]')
        .type('12345678')

        cy.get('input[name="country"]')
        .type('Norway')

        cy.get('input[name="city"]')
        .type('Trondheim')

        cy.get('input[name="street_address"]')
        .type('Road 2b')

        cy.get('#btn-create-account').click()
        cy.url().should('eq', 'https://secfit-frontend-ml.herokuapp.com/workouts.html')
    })


    it('Test (password both) input blank', () => {
        let user ='_' + Math.random().toString(36).substr(2, 9);
        cy.get('input[name="username"]')
        .type(user)
       
      
        cy.get('input[name="email"]')
        .type('mail@mail.com')
    
    /*
        cy.get('input[name="password"]')
        .type('passord123')

        cy.get('input[name="password1"]')
        .type('passord123')

    */

        cy.get('input[name="phone_number"]')
        .type('12345678')

        cy.get('input[name="country"]')
        .type('Norway')

        cy.get('input[name="city"]')
        .type('Trondheim')

        cy.get('input[name="street_address"]')
        .type('Road 2b')

        cy.get('#btn-create-account').click()
        cy.get('.alert').should('be.visible').should('contain', 'Registration failed!').should('contain', 'This field may not be blank.');
    })

    it('Test (password first) input blank', () => {
        let user ='_' + Math.random().toString(36).substr(2, 9);
        cy.get('input[name="username"]')
        .type(user)
       
      
        cy.get('input[name="email"]')
        .type('mail@mail.com')
    
    /*
        cy.get('input[name="password"]')
        .type('passord123')

     */

        cy.get('input[name="password1"]')
        .type('passord123')

   

        cy.get('input[name="phone_number"]')
        .type('12345678')

        cy.get('input[name="country"]')
        .type('Norway')

        cy.get('input[name="city"]')
        .type('Trondheim')

        cy.get('input[name="street_address"]')
        .type('Road 2b')

        cy.get('#btn-create-account').click()
        cy.get('.alert').should('be.visible').should('contain', 'Registration failed!').should('contain', 'This field may not be blank.');
    })


    it('Test (password second) input blank', () => {
        let user ='_' + Math.random().toString(36).substr(2, 9);
        cy.get('input[name="username"]')
        .type(user)
       
      
        cy.get('input[name="email"]')
        .type('mail@mail.com')
    
    
        cy.get('input[name="password"]')
        .type('passord123')
     /*
        cy.get('input[name="password1"]')
        .type('passord123')

    */
        cy.get('input[name="phone_number"]')
        .type('12345678')

        cy.get('input[name="country"]')
        .type('Norway')

        cy.get('input[name="city"]')
        .type('Trondheim')

        cy.get('input[name="street_address"]')
        .type('Road 2b')

        cy.get('#btn-create-account').click()
        cy.get('.alert').should('be.visible').should('contain', 'Registration failed!').should('contain', 'This field may not be blank.');
    })



    // Assumens this fails, but it does not :// very big bug yes.
    it('Test (different password in each password field) input blank', () => {
        let user ='_' + Math.random().toString(36).substr(2, 9);
        cy.get('input[name="username"]')
        .type(user)
       
      
        cy.get('input[name="email"]')
        .type('mail@mail.com')
    
    
        cy.get('input[name="password"]')
        .type('passord123')
     
        cy.get('input[name="password1"]')
        .type('different')

    
        cy.get('input[name="phone_number"]')
        .type('12345678')

        cy.get('input[name="country"]')
        .type('Norway')

        cy.get('input[name="city"]')
        .type('Trondheim')

        cy.get('input[name="street_address"]')
        .type('Road 2b')

        cy.get('#btn-create-account').click()
        cy.get('.alert').should('be.visible').should('contain', 'Registration failed!').should('contain', 'This field may not be blank.');
    })


    
    it('Test phone_number input blank', () => {
        let user ='_' + Math.random().toString(36).substr(2, 9);

        cy.get('input[name="username"]')
        .type(user)
       
      
        cy.get('input[name="email"]')
        .type('mail@mail.com')
    
    
        cy.get('input[name="password"]')
        .type('passord123')
     
        cy.get('input[name="password1"]')
        .type('passord123')

        /*
        cy.get('input[name="phone_number"]')
        .type('12345678')
        */

        cy.get('input[name="country"]')
        .type('Norway')

        cy.get('input[name="city"]')
        .type('Trondheim')

        cy.get('input[name="street_address"]')
        .type('Road 2b')

        cy.get('#btn-create-account').click()
        cy.url().should('eq', 'https://secfit-frontend-ml.herokuapp.com/workouts.html')
    
    })

    it('Test country input blank', () => {
        let user ='_' + Math.random().toString(36).substr(2, 9);

        cy.get('input[name="username"]')
        .type(user)
       
      
        cy.get('input[name="email"]')
        .type('mail@mail.com')
    
    
        cy.get('input[name="password"]')
        .type('passord123')
     
        cy.get('input[name="password1"]')
        .type('passord123')

        
        cy.get('input[name="phone_number"]')
        .type('12345678')
       
/*
        cy.get('input[name="country"]')
        .type('Norway')
 */
        cy.get('input[name="city"]')
        .type('Trondheim')

        cy.get('input[name="street_address"]')
        .type('Road 2b')

        cy.get('#btn-create-account').click()
        cy.url().should('eq', 'https://secfit-frontend-ml.herokuapp.com/workouts.html')
    
    })

    it('Test city input blank', () => {
        let user ='_' + Math.random().toString(36).substr(2, 9);

        cy.get('input[name="username"]')
        .type(user)
       
      
        cy.get('input[name="email"]')
        .type('mail@mail.com')
    
    
        cy.get('input[name="password"]')
        .type('passord123')
     
        cy.get('input[name="password1"]')
        .type('passord123')

        
        cy.get('input[name="phone_number"]')
        .type('12345678')
       

        cy.get('input[name="country"]')
        .type('Norway')
 /*
        cy.get('input[name="city"]')
        .type('Trondheim')
*/
        cy.get('input[name="street_address"]')
        .type('Road 2b')

        cy.get('#btn-create-account').click()
        cy.url().should('eq', 'https://secfit-frontend-ml.herokuapp.com/workouts.html')
    
    })

    it('Test street_adress input blank', () => {
        let user ='_' + Math.random().toString(36).substr(2, 9);

        cy.get('input[name="username"]')
        .type(user)
       
      
        cy.get('input[name="email"]')
        .type('mail@mail.com')
    
    
        cy.get('input[name="password"]')
        .type('passord123')
     
        cy.get('input[name="password1"]')
        .type('passord123')

        
        cy.get('input[name="phone_number"]')
        .type('12345678')
       

        cy.get('input[name="country"]')
        .type('Norway')

        cy.get('input[name="city"]')
        .type('Trondheim')
 /*
        cy.get('input[name="street_address"]')
        .type('Road 2b')
*/
        cy.get('#btn-create-account').click()
        cy.url().should('eq', 'https://secfit-frontend-ml.herokuapp.com/workouts.html')
    
    })

    
})
