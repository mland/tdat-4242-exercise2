describe('Integration test of upload file in exercise', () => {
    beforeEach('', () => {
        cy.visit('https://secfit-frontend-ml.herokuapp.com/login.html')

        cy.get('input[name="username"]')
        .type('a')
        
        cy.get('input[name="password"]')
        .type('a')

        cy.get('#btn-login').click()

    })
    it('Test upload image', () => {
        cy.url()
         .should('be.equal', 'https://secfit-frontend-ml.herokuapp.com/workouts.html')

        cy.contains('Exercises').click()

        cy.get('#btn-create-exercise').click()

        cy.get('#inputName').type('Test')
        cy.get('#inputDescription').type('description')
        cy.get('#inputUnit').type('1')
        cy.get('#inputDuration').type('1')
        cy.get('#inputCalories').type('1')

        let filepath = 'cypress/integration/test_img.jpg';
        cy.get('input[type="file"]').selectFile(filepath)

        cy.get('#btn-ok-exercise').click()

        cy.url()
         .should('be.equal', 'https://secfit-frontend-ml.herokuapp.com/exercises.html')

        cy.contains('Test').click()

        cy.get('.main-img').find('img').should('have.attr', 'src').should('contain', 'test_img.jpg')

        cy.wait(1000)
        cy.get('#btn-edit-exercise').click()
        //delete element
        cy.get('#btn-delete-exercise').click()

        cy.url()
         .should('be.equal', 'https://secfit-frontend-ml.herokuapp.com/exercises.html')
        
        cy.should('not.contain', 'Test')
    })

    it('Test upload video', () => {
        cy.url()
         .should('be.equal', 'https://secfit-frontend-ml.herokuapp.com/workouts.html')

        cy.contains('Exercises').click()

        cy.get('#btn-create-exercise').click()

        cy.get('#inputName').type('Test')
        cy.get('#inputDescription').type('description')
        cy.get('#inputUnit').type('1')
        cy.get('#inputDuration').type('1')
        cy.get('#inputCalories').type('1')

        let filepath = 'cypress/integration/test_video.mp4';
        cy.get('input[type="file"]').selectFile(filepath)

        cy.get('#btn-ok-exercise').click()

        cy.url()
         .should('be.equal', 'https://secfit-frontend-ml.herokuapp.com/exercises.html')

        cy.contains('Test').click()

        cy.get('#video-link').should('contain', 'test_video.mp4')

        //cy.get('.main-img').find('img').should('have.attr', 'src').should('contain', 'test_img.jpg')

        cy.wait(1000)
        cy.get('#btn-edit-exercise').click()
        //delete element
        cy.get('#btn-delete-exercise').click()

        cy.should('not.contain', 'Test')

    })
})