

Cypress.Commands.add('login', (username, password) => {
  cy.visit('https://secfit-frontend-ml.herokuapp.com/login.html')
  //cy.contains('Log in').click()
  cy.get('input[name="username"]').type(username)
  cy.get('input[name="password"]').type(password)
  cy.get('#form-login')
  .get('#btn-login').click()

})

describe('Test for exercise', () => {
    
    beforeEach('Visits the secfit page on heroku', () => {
      
      cy.login('a', 'a')
           
      cy.wait(1000)

      cy.contains('Exercises').click()
    
      cy.wait(1000)
      cy.contains('Crunch').click({force: true})
      
    })

    //abov 50 chars is typed
    it('Test if the unit field length over 50 chars which is its boundery', () => {
        cy.wait(150)
        cy.get('#btn-edit-exercise').click()
        cy.wait(150)
        cy.get('#inputUnit').clear().type('6666666666666666666666666666666666666666666666666666666666')
        cy.wait(150)
        cy.get('#btn-ok-exercise').click({force: true})
        cy.wait(1000)

        cy.get('.alert').should('be.visible');

        cy.wait(1000)

    })

    it('Test if the unit field length within 50 chars which is its boundery of', () => {
      cy.wait(150)
      cy.get('#btn-edit-exercise').click()
      cy.wait(150)
      cy.get('#inputUnit').clear().type('66666666666666666666')
      cy.wait(150)
      cy.get('#btn-ok-exercise').click({force: true})
      cy.wait(1000)

      cy.get('#inputUnit').should('have.value','66666666666666666666');

      cy.wait(1000)
    })

    

    //This works even tho is should not
    it('test what happen if type in max integer value in duration', () => {
      cy.wait(150)
      cy.get('#btn-edit-exercise').click()
      cy.wait(100)
      cy.get('#inputDuration').clear().type(2147483648)
      cy.wait(150)
      cy.get('#btn-ok-exercise').click({force: true})
      cy.wait(500)
      cy.get('.alert').should('be.visible');

    })
    
    
    //This works even tho is should not
    it('test what happen if type in negtive value in duration field', () => {
      cy.wait(150)
      cy.get('#btn-edit-exercise').click()
      cy.wait(100)
      cy.get('#inputDuration').clear().type(-1)
      cy.wait(150)

      cy.get('#btn-ok-exercise').click({force: true})
      cy.wait(150)
      cy.get('.alert').should('be.visible');

    })

    it('Test we are able to edit the duration field', () => {
      cy.wait(150)
      cy.get('#btn-edit-exercise').click()
      cy.wait(150)
      cy.get('#inputDuration').clear().type('66')
      cy.wait(150)
      cy.get('#btn-ok-exercise').click({force: true})
      cy.wait(1000)

      cy.get('#inputDuration').should('have.value','66');

      cy.wait(1000)
    })


     //This works even tho is should not
     it('test what happen if type in max integer value in calories', () => {
      cy.wait(150)
      cy.get('#btn-edit-exercise').click()
      cy.wait(100)
      cy.get('#inputCalories').clear().type(2147483648)
      cy.wait(150)
      cy.get('#btn-ok-exercise').click({force: true})
      cy.wait(150)
      cy.get('.alert').should('be.visible');

    })

    

    //This works even tho is should not
    it('test what happen if type in negtive value in calories field', () => {
      cy.wait(150)
      cy.get('#btn-edit-exercise').click()
      cy.wait(100)
      cy.get('#inputCalories').clear().type(-1)
      cy.wait(150)
      cy.get('#btn-ok-exercise').click({force: true})
      cy.wait(150)
      cy.get('.alert').should('be.visible');

    })

    it('Test we are able to edit the calories field', () => {
      cy.wait(150)
      cy.get('#btn-edit-exercise').click()
      cy.wait(150)
      cy.get('#inputCalories').clear().type('66')
      cy.wait(150)
      cy.get('#btn-ok-exercise').click({force: true})
      cy.wait(1000)

      cy.get('#inputCalories').should('have.value','66');

      cy.wait(1000)
    })

    

  })