describe('Integration test of upload file in exercise', () => {
    beforeEach('Log in', () => {
        cy.visit('https://secfit-frontend-ml.herokuapp.com/login.html')

        cy.get('input[name="username"]')
        .type('a')
        
        cy.get('input[name="password"]')
        .type('a')

        cy.get('#btn-login').click()

    })
    it('Test set coach and view at homepage', () => {
        cy.url()
         .should('be.equal', 'https://secfit-frontend-ml.herokuapp.com/workouts.html')

        cy.get('#nav-mycoach').click({force: true})
        
        cy.wait(1000)
        cy.get('#button-edit-coach').click()
        
        cy.wait(1000)
        cy.get('#input-coach').clear()
        .type('maria')

        cy.get('#button-set-coach').click()

        cy.contains('Home').click()

        cy.url()
         .should('be.equal', 'https://secfit-frontend-ml.herokuapp.com/index.html')

        cy.get('#div-coachcontent').get('.mb-1').should('contain', 'a')

        cy.get('#div-coachcontent').contains('a').click()

        cy.url()
         .should('be.equal', 'https://secfit-frontend-ml.herokuapp.com/mycoach.html')
        
    })

    it('Test if change and removal of element is displayed at homepage', () => {
        cy.url()
         .should('be.equal', 'https://secfit-frontend-ml.herokuapp.com/workouts.html')
        
        //create a test element
        cy.contains('Exercises').click()
        cy.get('#btn-create-exercise').click()

        cy.get('#inputName').type('Test')
        cy.get('#inputDescription').type('description')
        cy.get('#inputUnit').type('1')
        cy.get('#inputDuration').type('1')
        cy.get('#inputCalories').type('1')

        cy.get('#btn-ok-exercise').click()
        cy.wait(200)
        cy.contains('Home').click()
        cy.url()
         .should('be.equal', 'https://secfit-frontend-ml.herokuapp.com/index.html')

        //check if element is on homepage
        cy.get('#div-exercisecontent').should('contain', 'Test')
        cy.get('#div-exercisecontent').contains('Test').click()

        cy.wait(1000)
        cy.get('#btn-edit-exercise').click()

        cy.get('#inputName').clear().type("Edited Test")

        cy.wait(1000)
        cy.get('#btn-ok-exercise').click()

        cy.contains('Home').click()
        cy.url()
         .should('be.equal', 'https://secfit-frontend-ml.herokuapp.com/index.html')

        cy.get('#div-exercisecontent').should('contain', 'Edited Test')
        cy.get('#div-exercisecontent').contains('Edited Test').click()

        cy.wait(1000)
        cy.get('#btn-edit-exercise').click()
        //delete element
        cy.get('#btn-delete-exercise').click()

        cy.wait(1000)
        cy.contains('Home').click()
        cy.url()
         .should('be.equal', 'https://secfit-frontend-ml.herokuapp.com/index.html')

        //check that element no longer is on the homepage
        cy.get('#div-exercisecontent').should('not.contain', 'Test')

    })
})