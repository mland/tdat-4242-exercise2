
//The amount of element in front list 
let amount = 5;
let nocontent = "There are currently no entries in this list";

async function fetchExerciseTypes(request) {
    let response = await sendRequest("GET", `${HOST}/api/exercises/`);
    let container = document.getElementById('div-exercisecontent');
    let exerciseTemplate = document.querySelector("#template-listexercise");
    let emptyexerciseTemplate = document.querySelector("#template-exerciseempty");
    
    if (response.ok) {
        let data = await response.json();
        
        let exercises = data.results;

        
        exercises.reverse();
        let latest = exercises.slice(0, amount);
        
        
        if(latest.length > 0){
                           
            latest.forEach(exercise => {
                const exerciseAnchor = exerciseTemplate.content.firstElementChild.cloneNode(true);
                exerciseAnchor.href = `exercise.html?id=${exercise.id}`;

                const h5 = exerciseAnchor.querySelector("h5");
                h5.textContent = exercise.name;
                
            
                const p = exerciseAnchor.querySelector("p");
                p.textContent = exercise.description;   

                container.appendChild(exerciseAnchor);
            });
        }else{
            const exerciseAnchor = emptyexerciseTemplate.content.firstElementChild.cloneNode(true);
            
            const p = exerciseAnchor.querySelector("p");
            p.textContent = nocontent;
            
            container.appendChild(exerciseAnchor);
        }     
    }

    return response;
}

async function fetchAthletes(){
    let user = await getCurrentUser();
    let container = document.getElementById('div-athletecontent');
    let athleteTemplate = document.querySelector("#template-listathlete");
    let emptyathleteTemplate = document.querySelector("#template-athleteempty");

    
    let temp = user.athletes;
    temp.reverse();
    latest = temp.slice(0, amount);
    
    if(latest.length > 0){
       
        
        for (let athleteUrl of latest) {
            const athleteAnchor = athleteTemplate.content.firstElementChild.cloneNode(true);
            athleteAnchor.href = `myathletes.html?`;

            let response = await sendRequest("GET", athleteUrl);
            let athlete = await response.json();

            const h5 = athleteAnchor.querySelector("h5");
                h5.textContent = athlete.username;

            
            container.appendChild(athleteAnchor);
        }
    }else{

        const athleteAnchor = emptyathleteTemplate.content.firstElementChild.cloneNode(true);
            
        const p = athleteAnchor.querySelector("p");
        p.textContent = nocontent;
        
        container.appendChild(athleteAnchor);

    }
}


async function fetchWorkouts() {
    let response = await sendRequest("GET", `${HOST}/api/workouts/`);
    let emptyworkoutTemplate = document.querySelector("#template-workoutempty");


    if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
    } else {
        let data = await response.json();

        let workouts = data.results;
        let container = document.getElementById('div-workoutcontent');
       
        
        // does not display a workout with the same name twice. need rewue of logic
        
      /*
        for(let i = 0; i < latest.length-1; i ++) {
            for(let j = 0; j < latest.length-1; j ++){
                if (latest[i].name === latest[j].name) {
                    console.log(i,latest[i].name, j,latest[j+1].name)

                        if(i != j){
                        latest.splice(i,1);
                            if(amount < latest.length){
                                
                            }
                        }
                    
                }else{
                    console.log(i, j,"nah")
                   
                }
            }   
        }
        
       */
        //reverse to get the most recent workouts
        
        workouts.reverse();
        let latest = workouts.slice(0, amount);

        if(latest.length > 0){

            latest.forEach(workout => {
                let templateWorkout = document.querySelector("#template-listworkout");
                
                let cloneWorkout = templateWorkout.content.cloneNode(true);

                let aWorkout = cloneWorkout.querySelector("a");
                aWorkout.href = `workout.html?id=${workout.id}`;

                let h5 = aWorkout.querySelector("h5");
                h5.textContent = workout.name;

                let localDate = new Date(workout.date);

                let table = aWorkout.querySelector("table");
                let rows = table.querySelectorAll("tr");
                rows[0].querySelectorAll("td")[1].textContent = localDate.toLocaleDateString(); // Date
                rows[1].querySelectorAll("td")[1].textContent = localDate.toLocaleTimeString(); // Time
                rows[2].querySelectorAll("td")[1].textContent = workout.owner_username; //Owner
                rows[3].querySelectorAll("td")[1].textContent = workout.exercise_instances.length; // Exercises

                container.appendChild(aWorkout);
            });
        }else{
            const workoutAnchor = emptyworkoutTemplate.content.firstElementChild.cloneNode(true);
            
            const p = workoutAnchor.querySelector("p");
            p.textContent = nocontent;
            
            container.appendChild(workoutAnchor);
        } 
        

        return latest;
    }
}


async function fetchMeals(ordering) {
    let response = await sendRequest("GET", `${HOST}/api/meals/?ordering=${ordering}`);
    let emptymealsTemplate = document.querySelector("#template-mealsempty");

    if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
    } else {
        let data = await response.json();

        let meals = data.results;
        let container = document.getElementById('div-content');

        meals.reverse();
        let latest = meals.slice(0, amount);
        
        if (latest.length > 0) {
            latest.forEach(meal => {
                let templateMeal = document.querySelector("#template-meal");
                let cloneMeal = templateMeal.content.cloneNode(true);

                let aMeal = cloneMeal.querySelector("a");
                aMeal.href = `meal.html?id=${meal.id}`;

                let h5 = aMeal.querySelector("h5");
                h5.textContent = meal.name;

                let localDate = new Date(meal.date);

                let table = aMeal.querySelector("table");
                let rows = table.querySelectorAll("tr");
                rows[0].querySelectorAll("td")[1].textContent = localDate.toLocaleDateString(); // Date
                rows[1].querySelectorAll("td")[1].textContent = localDate.toLocaleTimeString(); // Time
                rows[2].querySelectorAll("td")[1].textContent = meal.owner_username; //Owner

                container.appendChild(aMeal);
        
            });

        }else{
            
            const mealsAnchor = emptymealsTemplate.content.firstElementChild.cloneNode(true);
            
            const p = mealsAnchor.querySelector("p");
            p.textContent = nocontent;
            
            container.appendChild(mealsAnchor);

        }
        return meals;
    }
}


async function displayCurrentCoach() {
    let user = await getCurrentUser();
    let coach = null;
    let container = document.getElementById('div-coachcontent');
    let emptycoachTemplate = document.querySelector("#template-coachempty");

    if (user.coach) {
        response = await sendRequest("GET", user.coach);
        if (!response.ok) {
            let data = await response.json();
            let alert = createAlert("Could not retrieve coach!", data);
            document.body.prepend(alert);
        }
        let coach = await response.json();
        //let input = document.querySelector("#input-coach");

        //input.value = coach.username;

        
        let coachTemplate = document.querySelector("#template-coach");

        const exerciseAnchor = coachTemplate.content.firstElementChild.cloneNode(true);
        exerciseAnchor.href = `mycoach.html`;

        //const h5 = exerciseAnchor.querySelector("h5");
        //h5.textContent = exercise.name;

        const p = exerciseAnchor.querySelector("p");
        p.textContent = coach.username;   

        container.appendChild(exerciseAnchor);
        

    } else {
        const coachAnchor = emptycoachTemplate.content.firstElementChild.cloneNode(true);
            
        const p = coachAnchor.querySelector("p");
        p.textContent = nocontent;
        
        container.appendChild(coachAnchor);
    }
}



window.addEventListener("DOMContentLoaded", async () => {
    let ordering = "-date";
    //kan duplisere kode, om koden blir hentet fra de respektable JS filene vil man få en console errror på buttons
    await displayCurrentCoach();

    let response = await fetchExerciseTypes();

    await fetchAthletes();

    let meals = await fetchMeals(ordering);
/*
    const urlParams = new URLSearchParams(window.location.search);
    if (urlParams.has('ordering')) {
        let aSort = null;
        ordering = urlParams.get('ordering');
        if (ordering == "name" || ordering == "owner" || ordering == "date") {
                let aSort = document.querySelector(`a[href="?ordering=${ordering}"`);
                aSort.href = `?ordering=-${ordering}`;
        } 
    } 

     let currentSort = document.querySelector("#current-sort");
     currentSort.innerHTML = (ordering.startsWith("-") ? "Descending" : "Ascending") + " " + ordering.replace("-", "");
*/
    let currentUser = await getCurrentUser();
    // grab username
    if (ordering.includes("owner")) {
        ordering += "__username";
    }

    let workouts = await fetchWorkouts();
    
    let tabEls = document.querySelectorAll('a[data-bs-toggle="list"]');
    for (let i = 0; i < tabEls.length; i++) {
        let tabEl = tabEls[i];
        tabEl.addEventListener('show.bs.tab', function (event) {
            let workoutAnchors = document.querySelectorAll('.workout');
            for (let j = 0; j < workouts.length; j++) {
                // I'm assuming that the order of workout objects matches
                // the other of the workout anchor elements. They should, given
                // that I just created them.
                let workout = workouts[j];
                let workoutAnchor = workoutAnchors[j];

                switch (event.currentTarget.id) {
                    case "list-my-workouts-list":
                        if (workout.owner == currentUser.url) {
                            workoutAnchor.classList.remove('hide');
                        } else {
                            workoutAnchor.classList.add('hide');
                        }
                        break;
                    case "list-athlete-workouts-list":
                        if (currentUser.athletes && currentUser.athletes.includes(workout.owner)) {
                            workoutAnchor.classList.remove('hide');
                        } else {
                            workoutAnchor.classList.add('hide');
                        }
                        break;
                    case "list-public-workouts-list":
                        if (workout.visibility == "PU") {
                            workoutAnchor.classList.remove('hide');
                        } else {
                            workoutAnchor.classList.add('hide');
                        }
                        break;
                    default :
                        workoutAnchor.classList.remove('hide');
                        break;
                }
            }
        });
    }
    

    
});
