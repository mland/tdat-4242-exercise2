let cancelButton;
let okButton;
let deleteButton;
let editButton;
let oldFormData;
let removeImgButton;
let uploadInput;

function getFormFieldList(form) {
    let list = [];
    for (let field of form) {
        if (field.name != "") {
            list.push(field.name);
        }
    }

    return list;
}

function validateMuscleGroup(type){
    let validTypes = ["Legs", "Chest", "Back", "Arms", "Abdomen", "Shoulders"]

    type = validTypes.includes(type) ? type : undefined;
    return type;
}

function hideExerciseFormButtons() {
    setReadOnly(true, "#form-exercise");

    okButton.className += " hide";
    deleteButton.className += " hide";
    cancelButton.className += " hide";
    uploadInput.className += " hide";
    removeImgButton.className += " hide";
    editButton.className = editButton.className.replace(" hide", "");

    cancelButton.removeEventListener("click", handleCancelButtonDuringEdit);
}

function deleteExerciseFormData(formData){
    for(let field of formData){
        formData.delete(field[0])
    }
}

function handleCancelButtonDuringEdit() {
    document.querySelector("select").setAttribute("disabled", "")

    hideExerciseFormButtons();

    let form = document.querySelector("#form-exercise");

    for (let field of form) {
        if(field.name != 'files' && oldFormData.has(field.name)) {  
            field.value = oldFormData.get(field.name);
        }
    }
 
    deleteExerciseFormData(oldFormData);

}

async function createExercise() {
    document.querySelector("select").removeAttribute("disabled")
    let form = document.querySelector("#form-exercise");
    let formFieldList = getFormFieldList(form);
    let formData = new FormData(form);
    let submitForm = new FormData();

    for (let field in formFieldList) {
        if (field != "files"){
            submitForm.append(formFieldList[field], formData.get(formFieldList[field]));
        }
    }

    for (let file of formData.getAll("files")) {
        submitForm.append("files", file);
    }

    let response = await sendRequest("POST", `${HOST}/api/exercises/`, submitForm, "");

    if (response.ok) {
        window.location.replace("exercises.html");
    } else {
        let data = await response.json();
        let alert = createAlert("Could not create new exercise!", data);
        document.body.prepend(alert);
    }
}

function handleEditExerciseButtonClick() {
    setReadOnly(false, "#form-exercise");

    document.querySelector("select").removeAttribute("disabled")

    editButton.className += " hide";
    okButton.className = okButton.className.replace(" hide", "");
    cancelButton.className = cancelButton.className.replace(" hide", "");
    deleteButton.className = deleteButton.className.replace(" hide", "");
    uploadInput.className = uploadInput.className.replace(" hide", "");
    removeImgButton.className = removeImgButton.className.replace(" hide", "")

    cancelButton.addEventListener("click", handleCancelButtonDuringEdit);

    let form = document.querySelector("#form-exercise");
    oldFormData = new FormData(form);
}

async function deleteExercise(id) {
    
    let response = await sendRequest("DELETE", `${HOST}/api/exercises/${id}/`);
    if (!response.ok) {
        let data = await response.json();
        let alert = createAlert(`Could not delete exercise ${id}`, data);
        document.body.prepend(alert);
    } else {
        window.location.replace("exercises.html");
    }
}

function handleRetriveFilesInExercise(exerciseData){
    const currentImageFileElement = document.querySelector("#current");
    let isFirstImg = true;

    for (let file of exerciseData.files) {
        
        removeImgButton.addEventListener("click", () => handleDeleteImgClick(file.id));
        
        if(file.file.endsWith(".mp4")){
            
            document.getElementById("div-video").className = "";
            let videoLink = document.getElementById("video-link");
            videoLink.textContent = file.file;
            videoLink.href = file.file;
            
        }else{
            let a = document.createElement("a");
            a.href = file.file;
            let pathArray = file.file.split("/");
            a.text = pathArray[pathArray.length - 1];
            a.className = "me-2";
    
            let img = document.createElement("img");
            img.src = file.file;

            if(isFirstImg){
                currentImageFileElement.src = file.file;
                isFirstImg = false;
            }
        }
    }
}

async function retrieveExercise(id) {
    let response = await sendRequest("GET", `${HOST}/api/exercises/${id}/`);

    if (!response.ok) {
        let data = await response.json();
        let alert = createAlert("Could not retrieve exercise data!", data);
        document.body.prepend(alert);
    } else {
        document.querySelector("select").removeAttribute("disabled")
        let exerciseData = await response.json();

        let form = document.querySelector("#form-exercise");
        let formData = new FormData(form);

        for (let key of formData.keys()) {
            let selector;
            if (key !== "muscleGroup") {
                selector = `input[name="${key}"], textarea[name="${key}"]`
            } else {
                selector = `select[name=${key}]`
            }
            
            let input = form.querySelector(selector);
            let newVal = exerciseData[key];
          
            if (key != "files") {
                input.value = newVal;
            }
        }
        document.querySelector("select").setAttribute("disabled", "")
        uploadInput.className += " hide";
        
        handleRetriveFilesInExercise(exerciseData);

    }
}

async function updateExercise(id) {
    
    let form = document.querySelector("#form-exercise");
    let formData = new FormData(form);
    let formFieldList = getFormFieldList(form);

    let muscleGroupSelector = document.querySelector("select")
    muscleGroupSelector.removeAttribute("disabled")
    
    let submitForm = new FormData();

    for (let field in formFieldList) {
        if (field != "files"){
            submitForm.append(formFieldList[field], formData.get(formFieldList[field]));
        }
    }

    for (let file of formData.getAll("files")) {
        console.log("file", file);
        submitForm.append("files", file);
    }
  
    let response = await sendRequest("PUT", `${HOST}/api/exercises/${id}/`, submitForm, "");

    if (!response.ok) {
        let data = await response.json();
        let alert = createAlert(`Could not update exercise ${id}`, data);
        document.body.prepend(alert);
    } else {
        muscleGroupSelector.setAttribute("disabled", "")

        hideExerciseFormButtons();

        deleteExerciseFormData(oldFormData);
    }
}


async function handleDeleteImgClick (id) {
    
    let response = await sendRequest("DELETE", `${HOST}/api/exercise-files/${id}/`);
    
    if(!response.ok){
        let data = await response.json();
        let alert = createAlert("faild to delete file", data);
        document.body.prepend(alert);
    }else{
        location.reload();       
    }
}

window.addEventListener("DOMContentLoaded", async () => {
    
    
    cancelButton = document.querySelector("#btn-cancel-exercise");
    okButton = document.querySelector("#btn-ok-exercise");
    deleteButton = document.querySelector("#btn-delete-exercise");
    editButton = document.querySelector("#btn-edit-exercise");
    removeImgButton = document.querySelector("#btn-delete-image");
    uploadInput = document.querySelector("#input-add-image-video");
    oldFormData = null;

    const urlParams = new URLSearchParams(window.location.search);

    // view/edit
    if (urlParams.has('id')) {
        const exerciseId = urlParams.get('id');
        await retrieveExercise(exerciseId);

        editButton.addEventListener("click", handleEditExerciseButtonClick);
        deleteButton.addEventListener("click", (async (id) => deleteExercise(id)).bind(undefined, exerciseId));
        okButton.addEventListener("click", (async (id) => updateExercise(id)).bind(undefined, exerciseId));
    } 
    //create
    else {
        setReadOnly(false, "#form-exercise");
        editButton.className += " hide";
        okButton.className = okButton.className.replace(" hide", "");
        cancelButton.className = cancelButton.className.replace(" hide", "");

        okButton.addEventListener("click", async () => createExercise());
        cancelButton.addEventListener("click", () => window.location.replace("exercises.html"));
    }
});