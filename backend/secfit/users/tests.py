from django.test import TestCase

# Create your tests here.

from users.models import User
from users.serializers import UserSerializer


# Create your tests here.

class UserSerializerTestCase(TestCase):
    def setUp(self):
        

        self.user_attributes = {
            'username': 'testuser',
            'email': 'mail@mail.com',
            'password': 'secure',
            'phone_number': '12345678',
            'country': 'Country',
            'city': 'City',
            'street_address': 'road 1b'
        }

        self.serialized_data = {
            'username': 'datauser',
            'email': 'data@mail.com',
            'password': 'datasecure',
            'phone_number': '187654321',
            'country': 'dataCountry',
            'city': 'dataCity',
            'street_address': 'data road 1b'
        }

        self.user = User.objects.create(**self.user_attributes)
        self.serializer = UserSerializer(instance=self.user, context={'request': None})

    def test_contains_expected_fields(self):
        data = self.serializer.data

        self.assertEqual(set(data.keys()), set([
            "url",
            "id",
            "email",
            "username",
            "athletes",
            "phone_number",
            "country",
            "city",
            "street_address",
            "coach",
            "workouts",
            "coach_files",
            "athlete_files",
        ]))

    def test_create(self):
        user_obj = self.serializer.create(self.serialized_data)

        self.assertEqual(user_obj.username, 'datauser')
        self.assertEqual(user_obj.city, 'dataCity')


