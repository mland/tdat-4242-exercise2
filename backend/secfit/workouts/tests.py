"""
Tests for the workouts application.
"""

from django.test import TestCase
from django.test import RequestFactory
from workouts.permissions import *
from workouts.models import Workout, Exercise, ExerciseInstance
from users.models import User

import datetime
from django.utils.timezone import utc
now = datetime.datetime.utcnow().replace(tzinfo=utc)

# Create your tests here.

class IsOwnerTestCase(TestCase):
    def setUp(self):
        self.factory = RequestFactory()

        self.user = User.objects.create(username="Testpass", email="m@m.com", phone_number='9999999', country="Norway", city="Trondheim", street_address="Osloveien")
        Workout.objects.create(name="push-up", date=now, notes ="Super strong", owner = self.user, visibility = "public")

        self.user2 = User.objects.create(username="Testfail", email="m@m.com", phone_number='9999999', country="Norway", city="Trondheim", street_address="Osloveien")
        
        
    def test_IsOwner(self):
        workout = Workout.objects.get(name="push-up")
        user = User.objects.get(username="Testpass")
        user2 = User.objects.get(username="Testfail")

        request = self.factory.get('/')

        request.user = user
        permission = IsOwner.has_object_permission(self, request=request, view=None, obj=workout)
        self.assertTrue(permission)
        
        request.user = user2
        permission = IsOwner.has_object_permission(self, request=request, view=None, obj=workout)
        self.assertFalse(permission)


class IsOwnerOfWorkoutTestCase(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        
        self.user = User.objects.create(username="owner", email="m@m.com", phone_number='9999999', country="Norway", city="Trondheim", street_address="Osloveien")
        self.user2 = User.objects.create(username="notowner", email="m@m.com", phone_number='9999999', country="Norway", city="Trondheim", street_address="Osloveien")
        self.workout = Workout.objects.create(name="push-up", date= now, notes ="Super strong", owner = self.user, visibility = "public")

        self.exercise = Exercise.objects.create(name="jump", description = "jump 10 times", unit ="minutes")
        self.exercise_instance = ExerciseInstance.objects.create(workout = self.workout, exercise = self.exercise, sets = "420", number = "69")

    def test_has_permission(self):
        workout = Workout.objects.get(name="push-up")
        user = User.objects.get(username="owner")
        user2= User.objects.get(username="notowner")
        

        # if request method is not POST
        request = self.factory.get('/')
        request.user = user2
        permission = IsOwnerOfWorkout.has_permission(self, request=request, view=None)
        self.assertTrue(permission)
        
        request = self.factory.post('/api/workouts/')
        request.data = {"workout": "url/"+ str(workout.id) +"/"}
        
        # if user is owner of workout
        request.user = user
        permission = IsOwnerOfWorkout.has_permission(self, request=request, view=None)
        self.assertTrue(permission)

        # if user is not owner of workout
        request.user = user2
        request.data = {"workout": {}}
        permission = IsOwnerOfWorkout.has_permission(self, request=request, view=None)
        self.assertFalse(permission)

        

    def test_has_object_permission(self):
        workout = Workout.objects.get(name="push-up")
        user = User.objects.get(username="owner")
        user2= User.objects.get(username="notowner")

        exercise_instance = self.exercise_instance

        request = self.factory.get('/')
        
        request.user = user
        permission = IsOwnerOfWorkout.has_object_permission(self, request=request, view=None, obj=exercise_instance)
        self.assertTrue(permission)

        request.user = user2
        permission = IsOwnerOfWorkout.has_object_permission(self, request=request, view=None, obj=exercise_instance)
        self.assertFalse(permission)



class IsCoachAndVisibleToCoachTestCase(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        
        self.coach = User.objects.create(username="coach", email="m@m.com", phone_number='9999999', country="Norway", city="Trondheim", street_address="Osloveien")

        self.user = User.objects.create(username="testuser", email="m@m.com", phone_number='9999999', country="Norway", city="Trondheim", street_address="Osloveien", coach = self.coach )
        
        #self.user2 = User.objects.create(username="Testfail", email="m@m.com", phone_number='9999999', country="Norway", city="Trondheim", street_address="Osloveien")

        self.workout = Workout.objects.create(name="push-up", date= now, notes ="Super strong", owner = self.user, visibility = "public")

        


    def test_has_object_permission(self):
        
        coach = User.objects.get(username="coach")
        user = User.objects.get(username="testuser")
        workout = Workout.objects.get(name="push-up")

        request = self.factory.get('/')

        request.user = coach
        permission = IsCoachAndVisibleToCoach.has_object_permission(self, request=request, view=None, obj=workout)
        self.assertTrue(permission)
        
        request.user = user
        permission = IsCoachAndVisibleToCoach.has_object_permission(self, request=request, view=None, obj=workout)
        self.assertFalse(permission)



class IsCoachOfWorkoutAndVisibleToCoachTestCase(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
            
        self.coach = User.objects.create(username="coach", email="m@m.com", phone_number='9999999', country="Norway", city="Trondheim", street_address="Osloveien")

        self.user = User.objects.create(username="Testpass", email="m@m.com", phone_number='9999999', country="Norway", city="Trondheim", street_address="Osloveien", coach = self.coach )
                
        self.workout = Workout.objects.create(name="push-up", date= now, notes ="Super strong", owner = self.user, visibility = "PU")

        self.exercise = Exercise.objects.create(name="jump", description = "jump 10 times", unit ="minutes")
        self.exercise_instance = ExerciseInstance.objects.create(workout = self.workout, exercise = self.exercise, sets = "420", number = "69")
        
    
    def test_IsCoachOfWorkoutAndVisibleToCoachTestCase(self):
        
        coach = User.objects.get(username="coach")
        user = User.objects.get(username="Testpass")
        workout = Workout.objects.get(name="push-up")

        exercise_instant = self.exercise_instance
        
        request = self.factory.get('/')

        request.user = user

        permission = IsCoachOfWorkoutAndVisibleToCoach.has_object_permission(self, request=request, view=None, obj=exercise_instant)
        self.assertFalse(permission)

        request.user = coach
        permission = IsCoachOfWorkoutAndVisibleToCoach.has_object_permission(self, request=request, view=None, obj=exercise_instant)
        self.assertTrue(permission)



class IsPublicTestCase(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create(username="Testpass", email="m@m.com", phone_number='9999999', country="Norway", city="Trondheim", street_address="Osloveien")
        Workout.objects.create(name="push-up", date= now, notes ="Super strong", owner = self.user, visibility = "PU")
        
        Workout.objects.create(name="sit-up", date= now, notes ="Super strong", owner = self.user, visibility = "CO")
    
    def test_IsPublicTestCase(self):
        workout = Workout.objects.get(name="push-up")
        workout2 = Workout.objects.get(name="sit-up")

        permission = IsPublic.has_object_permission(self, request=None, view=None, obj=workout)
        self.assertTrue(permission)

        permission = IsPublic.has_object_permission(self, request=None, view=None, obj=workout2)
        self.assertFalse(permission)

class IsWorkoutPublicTestCase(TestCase):
    def setUp(self):
        self.factory = RequestFactory()

        self.user = User.objects.create(username="Testpass", email="m@m.com", phone_number='9999999', country="Norway", city="Trondheim", street_address="Osloveien")
        
        self.workout = Workout.objects.create(name="push-up", date= now, notes ="Super strong", owner = self.user, visibility = "PU")
        
        self.workout2 = Workout.objects.create(name="sit-up", date= now, notes ="Super strong", owner = self.user, visibility = "CO")

        self.exercise = Exercise.objects.create(name="jump", description = "jump 10 times", unit ="minutes")
        self.exercise_instance = ExerciseInstance.objects.create(workout = self.workout, exercise = self.exercise, sets = "420", number = "69")

        self.exercise_instance2 = ExerciseInstance.objects.create(workout = self.workout2, exercise = self.exercise, sets = "420", number = "69")

    def test_IsWorkoutPublicTestCase(self):
        workout = Workout.objects.get(name="push-up")
        workout2 = Workout.objects.get(name="sit-up")

        exercise_instant = self.exercise_instance
        exercise_instant2 = self.exercise_instance2

        permission = IsWorkoutPublic.has_object_permission(self, request=None, view=None, obj=exercise_instant)
        self.assertTrue(permission)

        permission = IsWorkoutPublic.has_object_permission(self, request=None, view=None, obj=exercise_instant2)
        self.assertFalse(permission)




class IsReadOnlyTestCase(TestCase):
    def setUp(self):
        self.factory = RequestFactory()

        self.user = User.objects.create(username="Testpass", email="m@m.com", phone_number='9999999', country="Norway", city="Trondheim", street_address="Osloveien")
        
    
    def test_IsReadOnlyTestCase(self):
        request = self.factory.get('/')
        user = User.objects.get(username="Testpass")

        request.user = user


        permission = IsReadOnly.has_object_permission(self, request=request, view=None, obj=None)
        self.assertTrue(permission)

        request = self.factory.post('/')

        permission = IsReadOnly.has_object_permission(self, request=request, view=None, obj=None)
        self.assertFalse(permission)